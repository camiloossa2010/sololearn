﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            #region While-Break
            /*
            int num = 0;
            while (num<20)
            {
                if (num == 5)
                    break;
                Console.WriteLine(num);
                Console.ReadLine();
                num++;
            }*/
            #endregion*/

            #region For-Continue
            /*for(int i=0;i<10;i++)
            {
                if (i == 5)
                    continue;
                Console.WriteLine(i);
                Console.ReadLine();
                
            }*/
            #endregion

            #region Operadores Logicos
            /*int Edad = 20;
            int Grado = 60;
            
            if (Edad > 16 && Edad < 80 && Grado > 50)
            {
                Console.WriteLine("Oye Aqui");
            }
            else
            {
                Console.WriteLine("Error");
            }
            Console.ReadLine();//El anterior codigo es con operador "Y" */

            /*
            int Edad = 30;
            int Puntaje = 45;
            if(Edad>20|| Puntaje>50)
            {
                Console.WriteLine("Bienvenido");
            }
            else
            {
                Console.Write("Error");
            }
            Console.ReadLine();El Anterior codigo es con operador "O" (||)*/

            /*int Edad =8;
            if(!(Edad>=16))
            {
                Console.WriteLine("Tu edad es Menor de 16");
                
            }
            else
            {
                Console.Write("Tu edad es mayor a 16");
            }
            Console.ReadLine();-- El anterior codigo era operador no Logico (!)*/
            #endregion

            #region Operador?
            /*int Edad = 15;
            string msg;
            msg = (Edad >= 18) ? "Bienvenido" : "Error";
            Console.Write(msg);
            Console.ReadLine();

            double Nota = 2.5;
            string Mensaje;
            Mensaje = (Nota >=4) ? "Aprobado" : "Reprobado";
            Console.WriteLine(Mensaje);
            Console.ReadLine();*/
            #endregion

            #region Calculadoras
            /*do
            {
                Console.Write("X = ");
                int x = Convert.ToInt32(Console.ReadLine());

                Console.Write("Y = ");
                int y = Convert.ToInt32(Console.ReadLine());

                int sum = x + y;
                Console.WriteLine("Resultado:{0}", sum);
            }

            while (true);

            do
            {
                Console.Write("X= ");// Codigo para escribir el valor de X
                string str = Console.ReadLine();//Codigo para crear la variable string que es donde va almacenada la palabra exit
                if (str == "exit" || str == "Exit")// se pone la condicion cuando la palabra sea "exit" salga del bucle
                    break;
                int x = Convert.ToInt16(str);//Conversion de la x en string
                Console.Write("Y= ");//Codigo para el valor de Y
                int y = Convert.ToInt32(Console.ReadLine());//Conversion de Y en string

                int sum = x + y;//operacion de x que suma a Y
                Console.WriteLine("Resultado es: {0}", sum);//Saca el valor + el resultado almacenado en la variable sum

            }
            while (true);*/
#endregion


        }
    }
}
